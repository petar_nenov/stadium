# To use in

## Docker container

### Download from docker repo

1 install docker [Docker](https://www.docker.com/)  
2 run commands in terminal

```bash
docker pull kanatunta/stadium:latest
docker run --rm -d -p 8080:80 kanatunta/stadium
```

3 navigate to localhost:8080

### Build from docker file

1 install docker  
2 run commands in terminal

```bash
docker build -t @dockeraccount/@containername:latest .
docker run --rm -d -p 8080:80 @dockeraccount/@containername
```

3 navigate to localhost:8080

## LiveServer in VScode

1 Clone repo  
2 Start liveserver
